package downloaderGUI;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import thread.ThreadMusic;
import thread.ThreadPicture;

public class DownloaderController {

    @FXML
    private TextField urlMusic;

    @FXML
    private TextField urlPicture;

    @FXML
    private Button downloadBtn;

    @FXML
    private void initialize() {
        urlMusic.setText("https://ru.hitmos.org/");
        urlPicture.setText("http://youfon.net/all/");
        downloadBtn.setOnAction(actionEvent -> {
            String url_1 = urlMusic.getText();
            String url_2 = urlPicture.getText();
            if (check(url_1, url_2)) {
                ThreadMusic threadMusic = new ThreadMusic(url_1, "https?://\\S+(?:wav|mp3)", 1, "src\\music\\music", "mp3");
                ThreadPicture threadPicture = new ThreadPicture(url_2, "https?://\\S+(?:jpg|jpeg|png)", 1, "src\\picture\\picture", "jpg");
                threadMusic.start();
                threadPicture.start();
            }
        });
    }

    private boolean check(String url1, String url2) {
        boolean permission = true;

        if (url1.equals("") || url2.equals("")) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ошибка");
            alert.setHeaderText(null);
            alert.setContentText("Поле №1 или №2 пустое!");
            alert.showAndWait();

            permission = false;
        }
        return permission;
    }
}
package read;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadExtractLinks {

    /**
     * Метод произвоит чтение html с сайта и находит в нём ссылки на музыку
     *
     * @param strUrl - сслыка на сайт
     * @param numLinks - кол-во ссылок
     * @return - список с ссылками
     */
    public static ArrayList<String> extractUrls(String strUrl, int numLinks, String expression) {
        String line;
        int i = 0;

        ArrayList<String> listLinks = new ArrayList<>();

        try {
            URL url = new URL(strUrl);

            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {

                while ((line = bufferedReader.readLine()) != null) {
                    Pattern pattern = Pattern.compile(expression);
                    Matcher matcher = pattern.matcher(line);

                    while (matcher.find() && i < numLinks) {
                        listLinks.add(matcher.group());
                        i++;
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listLinks;
    }
}

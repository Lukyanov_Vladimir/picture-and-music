package menuGUI;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuController {

    @FXML
    private Button downloader;

    @FXML
    private Button playMusic;

    @FXML
    private Button viewPicture;

    @FXML
    private void initialize() {
        ImageView imageView = new ImageView(new Image(getClass().getResourceAsStream("/assets/download_icon.png")));
        imageView.setFitWidth(32);
        imageView.setFitHeight(32);
        downloader.setGraphic(imageView);

        ImageView imageView2 = new ImageView(new Image(getClass().getResourceAsStream("/assets/music_icon.png")));
        imageView2.setFitWidth(32);
        imageView2.setFitHeight(32);
        playMusic.setGraphic(imageView2);

        ImageView imageView3 = new ImageView(new Image(getClass().getResourceAsStream("/assets/image_icon.png")));
        imageView3.setFitWidth(32);
        imageView3.setFitHeight(32);
        viewPicture.setGraphic(imageView3);

        downloader.setOnAction(new EventHandler<>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("/downloaderGUI/downloaderGUI.fxml"));
                    Stage stage = new Stage();
                    stage.setTitle("Загрузчик");
                    stage.setMinWidth(475);
                    stage.setMinHeight(205);
                    stage.getIcons().add(new Image("/assets/download_icon.png"));
                    stage.centerOnScreen();
                    stage.setScene(new Scene(root));
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        playMusic.setOnAction(new EventHandler<>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("/musicGUI/musicGUI.fxml"));
                    Stage stage = new Stage();
                    stage.setTitle("Проигрыватель");
                    stage.setMinWidth(475);
                    stage.setMinHeight(205);
                    stage.getIcons().add(new Image("/assets/music_icon.png"));
                    stage.centerOnScreen();
                    stage.setScene(new Scene(root));
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        viewPicture.setOnAction(new EventHandler<>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("/pictureGUI/pictureGUI.fxml"));
                    Stage stage = new Stage();
                    stage.setTitle("Просмотр картинок");
                    stage.setMinWidth(475);
                    stage.setMinHeight(205);
                    stage.getIcons().add(new Image("/assets/image_icon.png"));
                    stage.centerOnScreen();
                    stage.setScene(new Scene(root));
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}

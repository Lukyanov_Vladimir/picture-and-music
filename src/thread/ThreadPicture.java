package thread;

import downloader.Downloader;
import read.ReadExtractLinks;

import java.util.ArrayList;

public class ThreadPicture extends Thread {
    private String url, expression, path, typeFile;
    private int numLinks;

    public ThreadPicture(String url, String expression, int numLinks, String path, String typeFile) {
        this.url = url;
        this.expression = expression;
        this.path = path;
        this.typeFile = typeFile;
        this.numLinks = numLinks;
    }

    @Override
    public void run() {
        ArrayList<String> picture = ReadExtractLinks.extractUrls(url, numLinks, expression);
        Downloader downloader = new Downloader(picture, path, typeFile);
        downloader.startDownload();
        System.out.println("ThreadPicture закончил загрузку");
    }
}

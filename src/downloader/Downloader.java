package downloader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;

public class Downloader {
    private ArrayList<String> listLinks;
    private String path;
    private String typeFile;

    public Downloader(ArrayList<String> listLinks, String path, String typeFile) {
        this.listLinks = listLinks;
        this.path = path;
        this.typeFile = typeFile;
    }

    /**
     * Метод запускает скачивание файла
     */
    public void startDownload() {
        for (String listLink : listLinks) {
            for (int j = 1; ; j++) {
                String str = path + j + "." + typeFile;
                File file = new File(str);
                if (!file.exists()) {
                    downloadNIO(listLink, str);
                    break;
                }
            }
        }
    }

    /**
     * Метод осуществляет закачку файлов по ссылке
     *
     * @param strUrl - ссылка
     * @param fileUrl - String расположение куда и какой файл будет записан
     */
    private void downloadNIO(String strUrl, String fileUrl) {
        try {
            URL url = new URL(strUrl);

            try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
                 FileOutputStream stream = new FileOutputStream(fileUrl)) {

                stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}

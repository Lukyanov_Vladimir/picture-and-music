package musicGUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.util.Collections;

public class MusicController {

    @FXML
    private ComboBox<String> comboBox;

    @FXML
    private Button updateBtn;

    @FXML
    private Button playBtn;

    @FXML
    private Button pauseBtn;

    @FXML
    private void initialize() {
        ImageView imageView = new ImageView(new Image(getClass().getResourceAsStream("/assets/play_icon.png")));
        imageView.setFitWidth(50);
        imageView.setFitHeight(50);
        playBtn.setGraphic(imageView);

        ImageView imageView2 = new ImageView(new Image(getClass().getResourceAsStream("/assets/pause_icon.png")));
        imageView2.setFitWidth(50);
        imageView2.setFitHeight(50);
        pauseBtn.setGraphic(imageView2);

        ImageView imageView3 = new ImageView(new Image(getClass().getResourceAsStream("/assets/update_icon.png")));
        imageView3.setFitWidth(16);
        imageView3.setFitHeight(16);
        updateBtn.setGraphic(imageView3);

        checkDirectory();

        updateBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                checkDirectory();
            }
        });

        playBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            }
        });
    }

    private void checkDirectory() {
        File file = new File("src\\music");
        String[] nameFiles = file.list((dir, name) -> name.endsWith(".mp3") || name.endsWith(".wav"));

        ObservableList<String> names = FXCollections.observableArrayList();
        assert nameFiles != null;
        Collections.addAll(names, nameFiles);

        comboBox.setItems(names);
    }
}


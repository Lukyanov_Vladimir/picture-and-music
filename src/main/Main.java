package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {
    private static final String DirMusic = "src\\music";
    private static final String DirPicture = "src\\picture";

    public static void main(String[] args) {
        createDirectory(DirMusic);
        createDirectory(DirPicture);
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/menuGUI/menuGUI.fxml"));
        stage.setTitle("Меню");
        stage.setMinWidth(360);
        stage.setMinHeight(300);
        stage.getIcons().add(new Image("/assets/menu_icon.png"));
        stage.centerOnScreen();
        stage.setScene(new Scene(root));
        stage.show();
    }

    private static void createDirectory(String directory) {
        File file = new File(directory);
        if (!file.exists()) {
            file.mkdir();
        }
    }
}

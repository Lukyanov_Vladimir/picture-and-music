# Picture and Music
## Программа: скачивает аудиофайлы с сайта "https://ru.hitmos.org/" и изображения с сайта "http://youfon.net/all/", воспроизводит скачаенные аудиофайлы и выводит скаченное изображение.
## Инструкция:
### 1. Склонировать репозиторий в IntelliJ IDEA.
### 2. Запустить программу.
## Созданно с помощью:
### Cреда разработки программного обеспечения: IntelliJ IDEA
### Язык программирования: Java
## Сторонние библиотеки:
* ### javaFX
## Авторы: Лукьянов Владимир